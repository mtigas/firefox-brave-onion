# firefox-brave-onion

Firefox extension that adds Brave as a Firefox search engine, using
Brave's onion service,
`brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion`. (See
[here](https://brave.com/new-onion-service/).)

**Download the extension on the official Firefox Add-Ons site**:  
https://addons.mozilla.org/en-US/firefox/addon/brave-search-tor-onion/

---

**Note**: This extension is only required for desktop Firefox or Tor Browser. For Tor Browser or Firefox on Android, you can manually add the search engine through the settings, with the following "Search string to use":

```
https://search.brave4u7jddbv7cyviptqjc7jusxh72uik7zt6adtckl5f4nwy2v72qd.onion/search?q=%s
```


## License

mtigas/firefox-brave-onion  
Copyright (C) 2022, Mike Tigas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
